﻿using System;

namespace ConsoleApp1_Html_Parser
{
    class Article
    {
        public int Id { get; set; }
        public String Tittle { get; set; }
        public DateTime Date { get; set; }
        public String Info { get; set; }
        public byte[] Img { get; set; }
        public String Full { get; set; }
    }
}
