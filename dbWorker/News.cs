﻿using dbWorker;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace ConsoleApp1_Html_Parser
{
    class News
    {
        public int Id { get; set; }
        public String Tittle { get; set; }
        public DateTime Date { get; set; }
        public String Info { get; set; }
        public byte[] Img { get; set; }
        public String Full { get; set; }

        public override string ToString()
        {
            return string.Format("id:\n{0}\n", Id) +
                   string.Format("Tittle:\n{0}\n", Tittle) +
                   string.Format("Date:\n{0}\n", Date) +
                   string.Format("Info:\n{0}\n", Info) +
                   string.Format("Full:\n{0}\n", Full) +
            "imgBytes len:" + Img.Length;
        }
        public static void parse()
        {
            for (; ; )
            {
                try
                {
                    var now = DateTime.Now;
                    var dformat = "dd.MMMM.yyyy HH:mm:ss";
                    Console.WriteLine("wakeup at   " + now.ToString(dformat));
                    parseRSS();
                    now = DateTime.Now;
                    if (now.Hour >= 22 || now.Hour < 8)
                    {
                        if (now.Hour <= 24 && now.Hour >= 22)
                        {
                            var curr = now;
                            var start = new DateTime(curr.Year, curr.Month, curr.Day, 8, 0, 0);
                            var next = start.AddDays(1);
                            var sleep = next.Subtract(now);
                            Console.WriteLine("next run at " + now.Add(sleep).ToString(dformat));
                            Thread.Sleep(sleep);
                        }
                        else
                        {
                            var curr = now;
                            var start = new DateTime(curr.Year, curr.Month, curr.Day, 8, 0, 0);
                            var sleep = start.Subtract(now);
                            Console.WriteLine("next run at " + now.Add(sleep).ToString(dformat));
                            Thread.Sleep(sleep);
                        }
                    }
                    else
                    {
                        long sleep = 60000 * 36 + rand();
                        Console.WriteLine("next run at "+now.AddMilliseconds(sleep).ToString(dformat));
                        Thread.Sleep(TimeSpan.FromMilliseconds(sleep));
                    }
                }
                catch (Exception e)
                {
                    Log.Add(e, "main error");
                }
            }
        }

        private static int rand()
        {
            Random r = new Random();
            return r.Next(-8*60000, 8*60000);
        }

        public static void parseRSS()
        {
            Log.Add("start rss parser");
            var url = @"https://www.rzn.info/rss.xml";
            string rss = null;
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                rss = client.DownloadString(url);
                Log.Add("download ok");
            }
            var xml = XDocument.Parse(rss);
            XNamespace yandex = "http://news.yandex.ru";

            var last = GetLastDate();

            var items = xml.Root.Element("channel").Elements("item")
                .Where(x => DateTime.Parse(x.Element("pubDate").Value) > last)
                .Select(x => new News
            {
                Tittle = x.Element("title").Value,
                Date = DateTime.Parse(x.Element("pubDate").Value),
                Full = x.Element(yandex+"full-text").Value,
                Info = x.Element("description").Value,
                Img = downloadBytes(x.Element("enclosure").Attribute("url").Value)
            }).OrderByDescending(x=>x.Date);

            var info = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": selected " + items.Count();
            Console.WriteLine(info);
            Log.Add("selected " + items.Count());

            foreach(var item in items)
            {
                item.Insert();
            }
            Log.Add("end rss parser ok");
        }

        private static DateTime GetLastDate()
        {
            var context = new NewsContext();
            var d = context.news.Max(x => x.Date);
            return d;
        }
        private static byte[] downloadBytes(string url)
        {
            using(var wc = new WebClient())
            {
                try
                {
                    return wc.DownloadData(url);
                }
                catch (Exception)
                {
                    try
                    {
                        return wc.DownloadData(url);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
        }
   

        private void Insert()
        {
            if (check())
                using (var db = new NewsContext())
                {
                    try
                    {
                        db.news.Add(this);
                        db.SaveChanges();
                    }catch(DbUpdateException e)
                    {
                        Log.Add("db update ex " + this.Id + this.Tittle);
                        Console.WriteLine("db update ex " + " "+this.Tittle);
                    }
                }
            else
                Log.Add(this,"check exeption");
        }

        private bool check()
        {
            if (Tittle == null)
                return false;

            //optimization
            if (Tittle.Contains("RZN.info") || Tittle.ToLower().Contains("фото") || Tittle.ToLower().Contains("видео"))
                return false;
            //end

            if (Img == null)
                return false;
            if (Full == null)
                return false;
            if (Date == null)
                return false;
            return true;
        }
    }
}
