﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ConsoleApp1_Html_Parser;
using Newtonsoft.Json;

namespace dbWorker
{
    class Log
    {
        private static string log;
        private static string path;
        static Log()
        {
            path = Environment.GetEnvironmentVariable("USERPROFILE") + "\\Desktop\\.exceptions\\";
            log = path + "dbWorker.log";
            
            if (!Directory.Exists(path+"models\\"))
            {
                Directory.CreateDirectory(path+"models\\");
            }
        }
        public static void Add(Exception e, string info)
        {
            var date = DateTime.Now;

            var text = date + ": " + info + "\n";
            File.AppendAllText(log, text, Encoding.UTF8);

            var file = path + date.ToString("dd.mm.yyyy.HH.mm.ss") + ".json";
            var json = JsonConvert.SerializeObject(e);

            File.AppendAllText(file, json);
        }

        public static void Add(News news, string info)
        {
            var date = DateTime.Now;

            var text = date + ": " + info + "\n";
            File.AppendAllText(log, text, Encoding.UTF8);

            var file = path + "models\\" + date.ToString("dd.mm.yyyy.HH.mm.ss") + ".json";
            var json = JsonConvert.SerializeObject(news);

            File.AppendAllText(file, json);
            
        }

        public static void Add(string v)
        {
            var text = DateTime.Now.ToString() + ": " +v+"\n";
            File.AppendAllText(log, text, Encoding.UTF8);
        }
    }
}
