﻿using Microsoft.EntityFrameworkCore;

namespace ConsoleApp1_Html_Parser
{
    class NewsContext:DbContext
    {
        public DbSet<News> news { get; set; }
        public DbSet<Article> articles { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=;UserId=root;Password=;database=mydb;SslMode=none;");
        }
    }
}
